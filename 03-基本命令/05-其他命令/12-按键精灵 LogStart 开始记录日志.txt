LogStart 开始记录日志

功能
将内部调试日志与 TracePrint 调试输出 的内容记录到日志文件中

语法
LogStart 文件路径

参数
参数	数据类型	解释
文件路径	字符串	记录的文件路径

返回值
无

示例

LogStart "C:\a.log" //记录日志
For i = 0 To 3
    TracePrint "保存次数：" & i
    Call Plugin.Msg.ShowScrTXT(0, 0, 1024, 768, "屏幕内容填写：" & i, "0000FF")
Next
LogStop //记录日志停止
TracePrint "这句不会输出到日志里！"


BeginThread 多线程 //新开线程
LogStart "C:\a.log" //记录日志
For i = 0 To 3
    TracePrint "保存次数：" & i
    Call Plugin.Msg.ShowScrTXT(0, 0, 1024, 300, "屏幕内容填写：" & i, "0000FF")
Next
LogStop //记录日志停止
TracePrint "这句不会输出到日志里！"
Delay 3000
Sub 多线程()
    LogStart "C:\b.log" //记录日志
    For i = 0 To 3
        TracePrint "多线程保存次数：" & i
        Call Plugin.Msg.ShowScrTXT(0, 300, 1024, 600, "多线程屏幕内容填写：" & i, "0000FF")
    Next
    LogStop //记录日志停止
    TracePrint "这句不会输出到日志里！"
End Sub

备注
日志功能只针对每个线程有效，例如在一个独立线程中开启了日志功能（LogStart），就只会记录下这个线程中的日志信息，其他线程如果需要记录日志，需要重新调用LogStart命令。
如果多个线程或脚本中使用了同一个文件做为日志文件，一旦其中一个线程或者脚本关闭（LogStop）了日志功能，其他线程或者脚本中的日志信息也会关闭。
在线程或者脚本结束时，系统会自动关闭日志功能。