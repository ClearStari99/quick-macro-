Import 导入命令

功能
导入一个vbs文件或者dll插件文件

语法
Import 文件路径

参数
参数	数据类型	解释
文件路径	字符串	vbs文件或dll文件(不写绝对路径时, 默认相对路径为plugin目录)

返回值
无

示例
按键精灵电脑版解析json方法-》https://zimaoxy.com/b/t-916-1-1.html

备注
本命令适合熟悉VBScript语言的人使用