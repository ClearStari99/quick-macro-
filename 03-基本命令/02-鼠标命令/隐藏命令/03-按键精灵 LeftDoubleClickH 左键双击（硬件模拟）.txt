LeftDoubleClickH 左键双击（硬件模拟）

功能
硬件模拟双击鼠标左键

语法
LeftDoubleClickH 次数

参数
参数	数据类型	解释
次数	整数型	需要双击的次数
返回值
无

示例

//移动鼠标到100,100坐标
MoveToH 100, 100

//双击鼠标左键1次
LeftDoubleClickH 1

备注
仅支持PS(圆口)接口的鼠标
当 LeftDoubleClick 左键双击 失效情况下, 可以尝试使用本命令