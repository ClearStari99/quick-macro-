LeftDoubleClick 左键双击

功能
普通模拟双击鼠标左键

语法
LeftDoubleClick 次数

参数
参数	数据类型	解释
次数	整数型	需要双击的次数

返回值
无

示例

//移动鼠标到100,100坐标
MoveTo 100, 100

//双击鼠标左键1次
LeftDoubleClick 1

备注
如果遇到点击失败且确认坐标无误的情况下, 尝试以下几种方法解决
如果你是单击桌面图标发现没效果的话, 请退出或者卸载360, 腾讯管家之类的防护软件
使用 SetSimMode 设置模拟方式 统一修改模拟方式, 也可以使用 LeftDoubleClickS 左键双击（超级模拟） 或 LeftDoubleClickH 左键双击（硬件模拟） 单独测试
使用 MoveR 鼠标相对移动 相对移动1个坐标后, 再执行点击
使用两句 LeftClick 左键单击 实现双击