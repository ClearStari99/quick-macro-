FindColorEx 模糊找色

功能
在屏幕指定区域范围内从左到右, 从上到下查找指定的颜色

语法
FindColorEx x1, y1, x2, y2, 颜色值, 查找方式, 相似度, 返回坐标x, 返回坐标y

参数
参数	数据类型	解释
x1	整数型	屏幕查找区域左上角坐标x
y1	整数型	屏幕查找区域左上角坐标y
x2	整数型	屏幕查找区域右下角坐标x
y2	整数型	屏幕查找区域右下角坐标y
颜色值	字符串	要查找的16进制颜色, 格式为“BBGGRR”, 仅支持写一个颜色值
查找方式	整数型	0为从上往下, 从左往右找; 1为从中心往外围找
相似度	小数型	取0.3到1之间的小数, 数值越大越相似
返回坐标x	整数型变量	返回找到的坐标x, 如果没找到返回-1, 仅支持写变量名
返回坐标y	整数型变量	返回找到的坐标y, 如果没找到返回-1, 仅支持写变量名

返回值
无

示例

FindColorEx 300, 400, 500, 600, "0000FF", 0, 0.9, x, y
If x > -1 Then
    TracePrint "找到颜色了, 坐标为: x=" & x & ", y=" & y
Else 
    TracePrint "没找到颜色"
End If


//高级例子
//介绍: 支持多个颜色的限时找色命令
//颜色: 多个颜色以|隔开, 例如"000000|AABBCC", 其他参数与FindColorEx类似
//时长: 单位毫秒, 指定时间内循环查找, 找到立即返回
//返回: 返回数组数据, 格式为[x, y, 序号], 未找到返回[-1, -1, -1]
Function zmFindColor(x1, y1, x2, y2, 颜色, 相似度, 时长)
    Dim colors, i, x, y, t
    zmFindColor = Array(-1, -1, -1)
    colors = Split(颜色, "|")
    t = Plugin.Sys.GetTime()
    Do
        For i = 0 To UBound(colors)
            FindColorEx x1, y1, x2, y2, colors(i), 0, 相似度, x, y
            If x > - 1  Then 
                zmFindColor = Array(x, y, i)
                Exit Function
            End If
        Next
        Delay 10
    Loop While 时长 > Plugin.Sys.GetTime() - t
End Function

//调用例子: 3秒内在指定范围内查找颜色AABBCC或者014DE1, 任何一个找到都返回结果
Dim ret
ret = zmFindColor(100, 200, 400, 500, "AABBCC|014DE1", 0.9, 3000)
If ret(0) > - 1  Then 
    TracePrint "找到坐标x=" & ret(0) & ", 坐标y=" & ret(1) & ", 序号=" & ret(2)
Else 
    TracePrint "没有找到"
End If

备注
找色系列命令强烈建议缩小查找范围, 提高相似度, 因为颜色很容易出现相同点, 导致找到的颜色不是你想要的点