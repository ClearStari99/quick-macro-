IsObject 是否为对象

功能
判断参数是否可以转换为对象数据类型

语法
结果 = IsObject(数据)

参数
参数	数据类型	解释
数据	任意类型	任意有效数据, 一般填写变量

返回值
布尔型, 返回是否可以转换为对象数据类型, True表示是, False表示否

示例

Dim MyInt, MyCheck, MyObject
Set MyObject = Me           
MyCheck = IsObject(MyObject)  ' 返回 True。
MyCheck = IsObject(MyInt)     ' 返回 False。

备注
本命令是判断是否可以转换为对象数据类型, 并非判断是否为该类型!