Time 取当前时间

功能
获取当前系统的时间, 不含日期

语法
结果 = Time()

参数
无

返回值
时间日期型, 返回当前本地系统的时间

示例

Dim MyTime
MyTime = Time()    ' 返回当前系统时间。
TracePrint MyTime


备注
无