Date 取当前日期

功能
获取当前系统的日期, 不含时间

语法
结果 = Date()

参数
无

返回值
时间日期型, 返回当前本地系统的日期

示例
1
2
3
Dim MyDate
MyDate = Date()    ' MyDate 包含当前系统日期。
TracePrint MyDate


备注
无