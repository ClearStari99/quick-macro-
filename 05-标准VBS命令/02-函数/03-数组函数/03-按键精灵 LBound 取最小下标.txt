LBound 取最小下标

功能
获取数组的最小可用下标

语法
结果 = LBound(数组[, 维度])

参数
参数	数据类型	解释
数组	数组	任意有效数组
维度	整数型	可选, 指定数组的维度下标, 1表示第一维, 2表示第二维, 以此类推, 默认为1

返回值
整数型, 返回数组指定维度的最小下标

示例

Dim a(3)
TracePrint LBound(a)


备注
任何一维数组的最小下标都是0
LBound 取最小下标 与 UBound 取最大下标 是相似命令