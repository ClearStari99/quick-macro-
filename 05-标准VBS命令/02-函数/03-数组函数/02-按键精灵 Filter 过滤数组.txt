Filter 过滤数组

功能
按照指定条件过滤数组内容

语法
结果 = Filter(原数组, 搜索字符[, 是否包含[, 对比方式]])

参数
参数	数据类型	解释
原数组	数组	一维数组, 要在这个数组中搜索字符
搜索字符	字符串	要过滤查找的文本内容
是否包含	布尔型	可选, 为True时返回包含搜索字符的数组结果, 为False则返回不包含搜索字符的数组结果
对比方式	整数型	可选, 搜索比较方式, 0为区分大小写, 1为忽略大小写, 2为执行基于数据库（在此数据库中执行比较）中包含的信息的比较。

返回值
数组, 返回按照是否包含条件搜索字符的数组结果

示例

Dim MyIndex
Dim MyArray (3)
MyArray(0) = "Sunday"
MyArray(1) = "Monday"
MyArray(2) = "Tuesday"
MyIndex = Filter(MyArray, "Mon") 'MyIndex(0) 包含 "Monday"。

备注
无