Currency 货币型

介绍
Currency 是数值型的一种, 取值范围在 -922,337,203,685,477.5808 到 922,337,203,685,477.5807 之间, 常用于金融货币上的计算

默认值
Currency 的默认值为 0

示例

Dim a

a = CCur(0)

TracePrint TypeName(a)
